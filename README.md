# HBRS Grader

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/joergbrech%2Fhbrs_grader/master?urlpath=lab/tree/hbrs_grader%2Ftemplate%2Ftemplate.ipynb)
[![pipeline status](https://gitlab.com/joergbrech/hbrs_grader/badges/master/pipeline.svg)](https://gitlab.com/joergbrech/hbrs_grader/commits/master) 
![pipy version](https://img.shields.io/pypi/v/hbrs_grader) 

**A Jupyter Notebook workflow for grading exams** for everyone who hates spreadsheets as much as I do.

This workflow allows you to
 - import a csv-file *(e.g. obtained from DIAS if you are at the Hochschule Bonn-Rhein-Sieg)*, 
 - enter points for each student for an exam consisting of a specified number of assignments, 
 - decide on a passing grade and the limit of a *"perfect"* grade,
 - export the grades to csv *(e.g. for upload to DIAS if you are at the Hochschule Bonn-Rhein-Sieg)* and 
 - create publishable results with some additional exam statistics.

Check out [this example notebook](https://mybinder.org/v2/gl/joergbrech%2Fhbrs_grader/master?urlpath=lab/tree/hbrs_grader%2Ftemplate%2Ftemplate.ipynb). 
The [exported HTML-document](https://gitlab.com/joergbrech/hbrs_grader/-/jobs/artifacts/master/download?job=system_check) will not contain the code cells. 


Since the export format is HTML and most online teaching tools such as MOODLE or ILIAS support HTML, you can simply copy the HTML source to a text node of your online course.

## Installation

Run 

```bash
pip install hbrs_grader
```

## Usage

### Step 1/3: Create a Notebook from template

Run 

```bash
hbrs-grader create 2019_MyExam
```

to create a Jupyter notebook called `2010_MyExam.ipynb` in your current working directory.

### Step 2/3: Grade your exam

Execute the newly created notebook using `jupyter-notebook` or `jupyter-lab`:

```bash
jupyter-notebook ./2019_MyExam.ipynb
```

Then just follow the instructions in the code comments. You can add additional code cells if you like, just make sure that all code cells contain the following metadata

```json
{
    "hideCode": true,
    "hidePrompt": true
}
```

so that they don't show up in the published html.

### Step 3/3: Publish the results

Once you are done grading, you can export the notebook to HTML with the following command:

```bash
hbrs-grader export-notebook 2019_MyExam.ipynb
```

import pytest


@pytest.mark.parametrize("limits,stats_expected", [
    ((0.4, 0.85),
     {'Notenschnitt': 2.84,
      'Standardabweichung': 1.11,
      'Durchfallquote [%]': 10.53,
      'Mitschreiber': 19,
      'Beste Note': 1,
      '25%-Quantil': 2.15,
      '50%-Quantil (Median)': 3.,
      '75%-Quantil': 3.3,
      'Schlechteste Note': 5.
      }
     ),
    ((0.5, 1.),
     {'Notenschnitt': 3.55,
      'Standardabweichung': 1.01,
      'Durchfallquote [%]': 21.05,
      'Mitschreiber': 19,
      'Beste Note': 1.7,
      '25%-Quantil': 3.,
      '50%-Quantil (Median)': 3.7,
      '75%-Quantil': 4.,
      'Schlechteste Note': 5.
      }
     ),
    ((0.49, 0.5),
     {'Notenschnitt': 1.84,
      'Standardabweichung': 1.68,
      'Durchfallquote [%]': 21.05,
      'Mitschreiber': 19,
      'Beste Note': 1.,
      '25%-Quantil': 1.,
      '50%-Quantil (Median)': 1.,
      '75%-Quantil': 1.,
      'Schlechteste Note': 5.
     }
    )
])
def test_exam_statistics(parametrized_exam_fixture, limits, stats_expected):

    parametrized_exam_fixture.set_invalid(1098789)

    parametrized_exam_fixture.calc_grades(*limits)
    stats = parametrized_exam_fixture.exam_statistics()

    for key in stats_expected:
        assert(stats.loc[key][''] == stats_expected[key])


def test_assignment_statistics(exam_fixture):

    exam_fixture.calc_grades(0.2, 0.8)
    stats = exam_fixture.assignment_statistics()

    columns = ['Aufgabe 1', 'Aufgabe 2', 'Aufgabe 3', 'Aufgabe 4', 'Aufgabe 5', 'Alle Aufgaben']
    stats_expected = \
        {'Mittelwert': [12.68, 9.95, 16.11, 12.05, 9.21, 60],
         'Standardabweichung': [6.09, 6.35, 5.91, 7.23, 6.39, 17.38],
         'Niedrigste Einzelwertung': [0., 0., 0., 0., 0., 23.],
         '25%-Quantil': [10., 5., 13., 8., 4.5, 51.5],
         '50%-Quantil (Median)': [14., 10., 18., 12., 10., 59.],
         '75%-Quantil': [16.5, 16., 20.5, 20., 13.5, 70.5],
         'Höchste Einzelwertung': [21., 16., 21., 20., 21., 90.],
         'Erreichbare Punkte': [21., 16., 21., 20., 22., 100.],
         'Schwierigkeit': [0.6, 0.62, 0.77, 0.6, 0.42, 0.6],
         'Diskriminierungsindex': [0.52, 0.62, -0.09, 0.56, 0.52, 0.42]
        }
    for key in stats_expected:
        for i in range(0, len(columns)):
            assert(stats.loc[key][columns[i]] == stats_expected[key][i])

import pytest


def test_key_not_found(exam_fixture):
    with pytest.raises(IndexError):
        exam_fixture.set_invalid(99999)

    with pytest.raises(IndexError):
        exam_fixture.set(99999, 'Aufgabe 4', 0)


def test_col_not_found(exam_fixture):
    with pytest.raises(IndexError):
        exam_fixture.set(7367299, 'Aubifgabibebi 4', 0)

    with pytest.raises(IndexError):
        exam_fixture.set_key('MabitNr')


def test_wrong_limits(exam_fixture):
    with pytest.warns(UserWarning):
        exam_fixture.calc_grades(0.6, 0.4)

    with pytest.raises(ValueError):
        exam_fixture.calc_grades(0.6, -1)

    with pytest.raises(ValueError):
        exam_fixture.calc_grades(0.6, 2)

    with pytest.raises(ValueError):
        exam_fixture.calc_grades(-0.6, 0.5)

    with pytest.raises(ValueError):
        exam_fixture.calc_grades(0.6, -0.5)


def test_wrong_number_of_assignments(exam_fixture):
    with pytest.raises(ValueError):
        exam_fixture.enter_points(7367299, [2, 2])

    with pytest.raises(ValueError):
        exam_fixture.enter_points(7367299, [2, 2, 2, 2, 2, 2, 2])


def test_wrong_points(exam_fixture):
    with pytest.raises(ValueError):
        exam_fixture.enter_points(7367299, [2, 2, 3, 4, 500])

    with pytest.raises(ValueError):
        exam_fixture.enter_points(7367299, [2, 2, 3, -4, 5])

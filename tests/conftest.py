import pytest
from hbrs_grader import Exam


def grade_template_exam():
    ex = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')

    ex.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                       points=[21, 16, 21, 20, 22])

    # Grade the exams by entering points
    ex.enter_points(9283492, [21, 4, 12, 10, 12])
    ex.enter_points(1124835, [14, 14, 16, 20, 15])
    ex.enter_points(4450683, [10, 10, 20, 15, 0])
    ex.enter_points(1134928, [7, 0, 21, 20, 2])
    ex.enter_points(2234643, [0, 0, 17, 0, 6])
    ex.enter_points(7645764, [10, 16, 20, 12, 10])
    ex.enter_points(2245675, [21, 16, 6, 0, 10])
    ex.enter_points(1111093, [10, 16, 0, 20, 10])
    ex.enter_points(7777878, [12, 16, 21, 10, 0])
    ex.enter_points(2234453, [19, 10, 21, 18, 20])
    ex.enter_points(2346543, [14, 10, 21, 0, 0])
    ex.enter_points(1345677, [11, 15, 19, 8, 15])
    ex.enter_points(4377298, [9, 10, 14, 6, 5])
    ex.enter_points(2947820, [15, 6, 10, 10, 15])
    ex.enter_points(2947729, [0, 0, 20, 8, 4])
    ex.enter_points(3652739, [16, 0, 21, 20, 12])
    ex.enter_points(2937749, [17, 16, 18, 12, 9])
    ex.enter_points(9773921, [20, 14, 11, 20, 9])
    ex.enter_points(7367299, [15, 16, 18, 20, 21])

    # explicitly state that some students did not show up or hand anything in
    ex.set_invalid(1098789)

    return ex


def grade_template_exam_workload_share_students():
    ############
    # Grader 1 #
    ############
    ex1 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex1.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                       points=[21, 16, 21, 20, 22])

    # Grade the exams by entering points
    ex1.enter_points(9283492, [21, 4, 12, 10, 12])
    ex1.enter_points(1124835, [14, 14, 16, 20, 15])
    ex1.enter_points(4450683, [10, 10, 20, 15, 0])
    ex1.enter_points(1134928, [7, 0, 21, 20, 2])
    ex1.enter_points(2234643, [0, 0, 17, 0, 6])

    ex1.set_invalid(1098789)

    ex1.write_csv('tests/exam_grader_1.csv')

    ############
    # Grader 2 #
    ############
    ex2 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')

    ex2.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                        points=[21, 16, 21, 20, 22])

    # Grade the exams by entering points
    ex2.enter_points(7645764, [10, 16, 20, 12, 10])
    ex2.enter_points(2245675, [21, 16, 6, 0, 10])
    ex2.enter_points(1111093, [10, 16, 0, 20, 10])
    ex2.enter_points(7777878, [12, 16, 21, 10, 0])
    ex2.enter_points(2234453, [19, 10, 21, 18, 20])

    ex2.write_csv('tests/exam_grader_2.csv')

    ############
    # Grader 3 #
    ############
    ex3 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')

    ex3.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                        points=[21, 16, 21, 20, 22])

    # Grade the exams by entering points
    ex3.enter_points(2346543, [14, 10, 21, 0, 0])
    ex3.enter_points(1345677, [11, 15, 19, 8, 15])
    ex3.enter_points(4377298, [9, 10, 14, 6, 5])
    ex3.enter_points(2947820, [15, 6, 10, 10, 15])
    ex3.enter_points(2947729, [0, 0, 20, 8, 4])

    ex3.write_csv('tests/exam_grader_3.csv')

    ############
    # Grader 4 #
    ############
    ex4 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')

    ex4.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                        points=[21, 16, 21, 20, 22])

    # Grade the exams by entering points
    ex4.enter_points(3652739, [16, 0, 21, 20, 12])
    ex4.enter_points(2937749, [17, 16, 18, 12, 9])
    ex4.enter_points(9773921, [20, 14, 11, 20, 9])
    ex4.enter_points(7367299, [15, 16, 18, 20, 21])

    ex4.write_csv('tests/exam_grader_4.csv')

    ###############
    # Aggregation #
    ###############
    ex = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex.add_data(["tests/exam_grader_" + str(i) + ".csv" for i in range(1, 5)])
    ex.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                       points=[21, 16, 21, 20, 22])

    return ex


def grade_template_exam_workload_share_assignments():
    ############
    # Grader 1 #
    ############
    ex1 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex1.add_assignments('Aufgabe 1', 21)

    # Grade the exams by entering points
    ex1.enter_points(9283492, 21)
    ex1.enter_points(1124835, 14)
    ex1.enter_points(4450683, 10)
    ex1.enter_points(1134928, 7)
    ex1.enter_points(2234643, 0)
    ex1.enter_points(7645764, 10)
    ex1.enter_points(2245675, 21)
    ex1.enter_points(1111093, 10)
    ex1.enter_points(7777878, 12)
    ex1.enter_points(2234453, 19)
    ex1.enter_points(2346543, 14)
    ex1.enter_points(1345677, 11)
    ex1.enter_points(4377298, 9)
    ex1.enter_points(2947820, 15)
    ex1.enter_points(2947729, 0)
    ex1.enter_points(3652739, 16)
    ex1.enter_points(2937749, 17)
    ex1.enter_points(9773921, 20)
    ex1.enter_points(7367299, 15)

    ex1.write_csv('tests/exam_grader_1.csv')

    ############
    # Grader 2 #
    ############
    ex2 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex2.add_assignments('Aufgabe 2', 16)

    # Grade the exams by entering points
    ex2.enter_points(9283492, 4)
    ex2.enter_points(2346543, 10)
    ex2.enter_points(1345677, 15)
    ex2.enter_points(4377298, 10)
    ex2.enter_points(2937749, 16)
    ex2.enter_points(9773921, 14)
    ex2.enter_points(1134928, 0)
    ex2.enter_points(2234643, 0)
    ex2.enter_points(2947820, 6)
    ex2.enter_points(2947729, 0)
    ex2.enter_points(3652739, 0)
    ex2.enter_points(1124835, 14)
    ex2.enter_points(4450683, 10)
    ex2.enter_points(1111093, 16)
    ex2.enter_points(7777878, 16)
    ex2.enter_points(7645764, 16)
    ex2.enter_points(2245675, 16)
    ex2.enter_points(7367299, 16)
    ex2.enter_points(2234453, 10)

    ex2.write_csv('tests/exam_grader_2.csv')

    ############
    # Grader 3 #
    ############
    ex3 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex3.add_assignments('Aufgabe 3', 21)

    # Grade the exams by entering points
    ex3.set(9283492, 'Aufgabe 3', 12)
    ex3.set(1124835, 'Aufgabe 3', 16)
    ex3.set(4450683, 'Aufgabe 3', 20)
    ex3.set(1134928, 'Aufgabe 3', 21)
    ex3.set(2234643, 'Aufgabe 3', 17)
    ex3.set(7645764, 'Aufgabe 3', 20)
    ex3.set(2245675, 'Aufgabe 3', 6)
    ex3.set(1111093, 'Aufgabe 3', 0)
    ex3.set(7777878, 'Aufgabe 3', 21)
    ex3.set(2234453, 'Aufgabe 3', 21)
    ex3.set(2346543, 'Aufgabe 3', 21)
    ex3.set(1345677, 'Aufgabe 3', 19)
    ex3.set(4377298, 'Aufgabe 3', 14)
    ex3.set(2947820, 'Aufgabe 3', 10)
    ex3.set(2947729, 'Aufgabe 3', 20)
    ex3.set(3652739, 'Aufgabe 3', 21)
    ex3.set(2937749, 'Aufgabe 3', 18)
    ex3.set(9773921, 'Aufgabe 3', 11)
    ex3.set(7367299, 'Aufgabe 3', 18)

    # explicitly state that some students did not show up or hand anything in
    ex3.set_invalid(1098789)

    ex3.write_csv('tests/exam_grader_3.csv')

    ############
    # Grader 4 #
    ############
    ex4 = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex4.add_assignments(['Aufgabe 4', 'Aufgabe 5'], [20, 22])

    # Grade the exams by entering points
    ex4.enter_points(9283492, [10, 12])
    ex4.enter_points(1124835, [20, 15])
    ex4.enter_points(4450683, [15, 0])
    ex4.enter_points(1134928, [20, 2])
    ex4.enter_points(2234643, [0, 6])
    ex4.enter_points(7645764, [12, 10])
    ex4.enter_points(2245675, [0, 10])
    ex4.enter_points(1111093, [20, 10])
    ex4.enter_points(7777878, [10, 0])
    ex4.enter_points(2234453, [18, 20])
    ex4.enter_points(2346543, [0, 0])
    ex4.enter_points(1345677, [8, 15])
    ex4.enter_points(4377298, [6, 5])
    ex4.enter_points(2947820, [10, 15])
    ex4.enter_points(2947729, [8, 4])
    ex4.enter_points(3652739, [20, 12])
    ex4.enter_points(2937749, [12, 9])
    ex4.enter_points(9773921, [20, 9])
    ex4.enter_points(7367299, [20, 21])

    ex4.write_csv('tests/exam_grader_4.csv')

    ###############
    # Aggregation #
    ###############
    ex = Exam(r'hbrs_grader/template/exam_in.csv', sep='\t')
    ex.add_data(["tests/exam_grader_" + str(i) + ".csv" for i in range(1, 5)])
    ex.add_assignments(header=['Aufgabe ' + str(i) for i in range(1, 6)],
                       points=[21, 16, 21, 20, 22])

    ex.set_invalid(1098789)
    return ex


@pytest.fixture()
def exam_fixture():
    return grade_template_exam()


@pytest.fixture(scope='function', params=[grade_template_exam,
                                          grade_template_exam_workload_share_students,
                                          grade_template_exam_workload_share_assignments])
def parametrized_exam_fixture(request):
    return request.param()

API Changes:
 - Rename `set_ne` to `set_invalid`
 - Replace `set_assignments` by `add_assignments`
 
New Features
 - Allow arbitrary assignment names, instead of prescribing a prefix for the column headers.
 - Allow custom grade labels instead of the German system
   (`['5.0', '4.0', '3.7', '3.3', '3.0', '2.7', '2.3', '2.0', '1.7', '1.3', '1.0', 'n.e.']`). Now pass-fail exams are
   possible with e.g. `set_grade_labels(['fail', 'pass', 'invalid'])` or the American grading systems with e.g.
   `set_grade_labels(['F', 'D-', 'D', 'D+', 'C-', 'C', 'C+', 'B-', 'B', 'B+', 'A-', 'A', 'A+', 'n.a.'])`
 - Allow custom keys to refer to students

# v1.0.1
2019-09-16

Major changes
 - Publish to PyPi

Minor fixes
 - function names of library are PEP8 conform
 - fix small bugs associated with exam statistics

# v1.0
2019-09-15

 - first 1.x release!

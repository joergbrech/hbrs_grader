# -*- coding: iso-8859-1 -*-

import pandas as pd
import numpy as np
from collections import OrderedDict
from matplotlib.ticker import MaxNLocator

import io
import os
import warnings
import click

try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources
from . import template

from IPython.display import display, HTML

import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
from hide_code import HideCodeHTMLExporter, HideCodePDFExporter


def is_numeric(l):
    """
    helper function to check, if all elements of a list can be converted to numeric values
    """
    if not (isinstance(l, list) or isinstance(l, tuple)):
        lst = [l, ]
    else:
        lst = l

    for s in lst:
        try:
            float(s)
        except ValueError:
            return False
    return True


class Exam:
    """
    A class for grading an exam. It is a wrapper around a pandas DataFrame with some additional functionality.
    """

    # grades is a Pandas DataFrame that contains the grades of every person
    grades = None

    # the number of assignments in the exam
    assignments = OrderedDict()
    limit_pass = 0.4
    limit_perfect = 0.8
    key = None

    # default grade labels are from German upper education grading system
    grade_labels = ['5.0', '4.0', '3.7', '3.3', '3.0', '2.7', '2.3', '2.0', '1.7', '1.3', '1.0', 'ne']

    def add_data(self, csv_path, key=None, **kwargs):
        """
        Append the exam data from csv files. If there is a data conflict, the old data is retained for non-numeric
        values, while for numeric values the maximum of the old and new values is retained
        :param csv_path: a single csv file or a list of csv files
        :param key: A key that is used to refer to students (i.e. a column header)
        """

        if isinstance(csv_path, list):
            csvs = csv_path
        else:
            csvs = [csv_path, ]

        start_idx = 0
        if self.grades is None:
            self.grades = pd.read_csv(csvs[0], **kwargs)
            start_idx = 1

        # use first column as key into student list by default
        if key is None and self.key is None:
            self.key = self.grades.columns[0]
        elif key is not None:
            self.key = key

        # append data if there is a list of csv files
        for idx in range(start_idx, len(csvs)):
            new_data = pd.read_csv(csvs[idx], **kwargs)

            # Now merge the data. This is horribly slow, but I don't know how to do this with merge, join, combine...

            # divide new data's columns into those, that exist in self.grades and those that don't
            new_cols = []
            old_cols = []
            for col in new_data.columns:
                if col not in self.grades.columns:
                    new_cols.append(col)
                else:
                    old_cols.append(col)

            # go through each row of the new data
            for new_key in new_data[self.key]:
                if new_key not in list(self.grades[self.key]):
                    # we found a new row! take it completely
                    self.grades = self.grades.append(new_data[new_data[self.key] == new_key])
                else:
                    # we found an old row
                    for col in old_cols:
                        # got through all cols that exist and compare the values. Take maximum values if the data is
                        # numeric or the old data is nan, take the old data otherwise
                        try:
                            old_val = float(self.grades[self.grades[self.key] == new_key].iloc[0][col])
                            new_val = float(new_data[new_data[self.key] == new_key].iloc[0][col])
                            if pd.isnull(old_val) or (float(new_val) > float(old_val)):
                                idx = self.grades.index[self.grades[self.key] == new_key].to_list()
                                self.grades.at[idx, col] = new_data[new_data[self.key] == new_key].iloc[0][col]
                        except ValueError:
                            # type cannot be cast to float. For non_numeric data, we keep the old values
                            pass

                    for col in new_cols:
                        # take the new columns
                        idx = self.grades.index[self.grades[self.key] == new_key].to_list()
                        self.grades.at[idx, col] = new_data[new_data[self.key] == new_key].iloc[0][col]
                self.grades = self.grades.reset_index(drop=True)

        if 'Note' not in self.grades.columns:
            self.grades['Note'] = np.nan
        self.grades = self.grades.fillna(0)

    # Initialize exam from csv_file
    __init__ = add_data

    def set_key(self, col_name):
        """
        Sets the key with which we want to refer to a student. E.g. if "Matr.-Nr" is a column header in the imported
        csv file, set_key("Matr.-Nr.") would mean we would enter the points per using the students "Matr.-Nr". By
        default, the key is the name of the first column
        """
        if col_name not in self.grades.columns:
            raise IndexError('No column \"' + str(col_name) + '\"  found.')

        self.key = col_name

    def set_grade_labels(self, label_list):
        """
        use a custom set of grade labels, e.g. ['fail', 'pass', 'invalid'] for a fail-pass situation.

        The first grade will be the one for all failed exams. The second to last grade will be the best achievable
        grade. All grades in between will span the points between the passing limit and the limit for the "perfect"
        grade. The last element of the list is a label for an examl that is invalid.

        If there are only three elements in the list, the exam is graded as a pass-fail situation and the limit for the
        "perfect" grade is ignored.

        :param label_list: A list of category labels of minimum length = 3. The last element is the label for an invalid
        grade, the first to second to last labels are the grades ordered from worst to best, where the worst grade
        corresponds to a failed exam
        """
        if len(label_list) < 3:
            raise ValueError('There must be at least three grade labels, one for the failing grade, one for the \
                             passing grade and one for an invalid grade')
        self.grade_labels = label_list

    def add_assignments(self, header, points):
        """
        Adds columns with names specicified in the header argument.
        :param header: A name or list of names of the assignments
        :param points: points or list of points maximally achievable in the assignments
        """

        if isinstance(header, list) and isinstance(points, list):
            if not len(points) == len(header):
                raise IndexError('The list of points does not match the number of assignments')

            assignments_new = OrderedDict(zip(header, points))
        else:
            assignments_new = OrderedDict()
            assignments_new[header] = points

        for col in assignments_new.keys():
            if col not in self.grades:
                self.grades[col] = 0

        aux = self.assignments.copy()
        aux.update(assignments_new)
        self.assignments = aux

    def points_total(self):
        """
        :return: The total number of points achievable in this exam
        """
        pnts = 0
        for key in self.assignments:
            pnts += self.assignments[key]
        return pnts

    def assignment_name(self, i):
        """
        :param i:
        :return: name of assignment with index i
        """
        return list(self.assignments.items())[i][0]

    def assignment_points(self, i):
        """
        :param i:
        :return: achievable points of assignment with index i
        """
        return list(self.assignments.items())[i][1]

    def enter_points(self, key, points):
        """
        Enter points for all assignments for a student.
        :param key: student key
        :param points: A list of points
        """

        num_parts = len(self.assignments)
        if isinstance(points, list):

            if not len(points) == num_parts:
                raise ValueError('The list of points does not match the number of assignments')

            for i in range(0, num_parts):
                self.set(key, self.assignment_name(i), points[i])
        else:
            if num_parts > 1:
                raise ValueError('The list of points does not match the number of assignments')

            self.set(key, self.assignment_name(0), points)

    def set(self, key, col, value, append=False):
        """
        Modify a specific column for a student
        :param key: The key of the student
        :param col: The column header that shall be modified
        :param value: The value to insert
        :param append: If True, the value is appended as a string
        :return:
        """
        if key not in list(self.grades[self.key]):
            raise IndexError('No column with \"' + str(self.key) + '\" = ' + str(key) + ' found.')

        if col not in self.grades.columns:
            raise IndexError('No column \"' + str(col) + '\"  found.')

        if append:
            self.grades.loc[self.grades[self.key] == key, col] += ' ' + value
        else:
            self.grades.loc[self.grades[self.key] == key, col] = value

        if col in list(self.assignments.keys()):
            if value < 0 or value > self.assignments[col]:
                raise ValueError('The number of points for assignment {} must be between 0 and {}.'
                                 .format(col, self.assignments[col]))
            self.sum_points()  # this is done more than necessary, but hey...

    def sum_points(self):
        """
        Sum points over all assignments and write to "Punkte" column
        """
        self.grades['Punkte'] = self.grades[list(self.assignments.keys())].sum(1)

    def get(self, key):
        """
        :param key: student key
        :return: row in the table for a student
        """
        return self.grades[self.grades[self.key] == key]

    def get_ungraded(self):
        """
        :return: table of ungraded students
        """
        return self.grades[self.grades['Punkte'].isna() or self.grades['Punkte'] == 0]

    def get_graded(self):
        """
        :return: table of students with at least one point
        """
        return self.grades[self.grades['Punkte'] > 0]

    def set_invalid(self, key):
        """
        label exam of student with given key as invalid, e.g. because the student did not show up, cheated or did not
        hand anything in.
        """
        if key not in list(self.grades[self.key]):
            raise IndexError('No column with \"' + str(self.key) + '\" = ' + str(key) + ' found.')
        self.grades.loc[self.grades[self.key] == key, 'Punkte'] = 0
        self.grades.loc[self.grades[self.key] == key, 'Note'] = self.grade_labels[-1]

    def calc_grade_bins(self, limit_pass, limit_perfect):
        """
        helper function to calculate the point-bins for the grades, given the pass and perfect limits.
        For a pass-fail situation, only the pass grade is considered
        :param limit_pass: The limit of points above which an exam is considered "passed"
        :param limit_perfect: The limit of points above which an exam is considered as "perfect"
        :return: labels, bins for the point intervals of each grade
        """

        if limit_pass < 0 or limit_pass > 1.:
            raise ValueError('limit_pass must be a value between 0 and 1')
        if limit_perfect < 0 or limit_perfect > 1.:
            raise ValueError('limit_perfect must be a value between 0 and 1')

        labels = self.grade_labels[0:-1]
        bins = [0.]
        n_bins = len(self.grade_labels)-1
        points_total = self.points_total()
        if n_bins == 2:  # pass-fail
            bins.append(limit_pass*points_total)
            bins.append(points_total+1)
        else:
            if limit_perfect - limit_pass <= 0:
                warnings.warn('limit_perfect should be larger than limit_pass. For a pass-fail exam consider '
                              'overwriting the grade_labels of the exam.')
                bins.append(limit_pass * points_total)
                labels = [self.grade_labels[0], self.grade_labels[-2]]
            else:
                for i in range(0, n_bins-1):
                    bins.append(limit_pass * points_total
                                + i * (limit_perfect - limit_pass) * points_total / (n_bins-2))
            # append points_total + 1 for the case that the best grade is only given for a true perfect score. In this
            # case we create a nonempty bin so that pandas does not bitch around about duplicates.
            bins.append(points_total + 1)
        return labels, bins

    def calc_grades(self, limit_pass, limit_perfect=1., relative=True):
        """
        Calculate the grades based on the limits to pass the exam and the limit for a perfect grade. By default the
        limits are assumed to be relative, i.e. between 0 or 1.
        :param limit_pass: limit to pass the exam
        :param limit_perfect: limit for a perfect grade. Ignored, if there are only two valid grade labels
        :param relative: True, if input is relative, i.e. between 0 and 1
        :return:
        """

        self.sum_points()  # just to be sure, sum all points again

        points_total = self.points_total()
        if not relative:
            limit_pass = limit_pass / points_total
            limit_perfect = limit_pass / points_total
        self.limit_pass = limit_pass
        self.limit_perfect = limit_perfect
        labels, bins = self.calc_grade_bins(limit_pass, limit_perfect)

        # only edit valid students
        writers = (self.grades['Note'] != self.grade_labels[-1])
        self.grades.loc[writers, 'Note'] = \
            pd.cut(self.grades[writers]['Punkte'], bins=bins, labels=labels, right=False)

    def grade_point_table(self, resolution=0.5):
        """
        Create a table to display the conversion from points to grades
        """
        labels, bins = self.calc_grade_bins(self.limit_pass, self.limit_perfect)
        a = np.array([bins[:-1]])
        b = np.array([bins[1:]])

        # ceil to nearest half point
        a = np.ceil(a / resolution) * resolution
        b = np.ceil(b / resolution) * resolution

        c = np.concatenate((a.T, b.T), axis=1)
        cols = pd.MultiIndex.from_product([['Punkte'], ['>=', '<']])
        tab = pd.DataFrame(data=c, index=labels, columns=cols).applymap(str)
        perfect_grade = self.grade_labels[-2]
        tab.loc[perfect_grade, ('Punkte', '<')] = ""  # clear < upper limit for highest grade
        return tab

    def assignment_statistics(self):
        """
        DataFrame with detailed statistics for the individual assignments
        """
        writers = self.grades['Note'] != self.grade_labels[-1]
        columns = list(self.assignments.keys())
        columns.append('Punkte')
        stats = self.grades[writers][columns]

        points = list(self.assignments.values())
        points.append(self.points_total())

        # calculate discrimination index
        # TODO: KR 20 reliability?
        points_sorted = stats.sort_values(by='Punkte')
        num_writers = writers.sum()
        twentyseven_percent = int(np.round(0.27 * num_writers))
        lowest = points_sorted.iloc[:twentyseven_percent]
        highest = points_sorted.iloc[-twentyseven_percent:]
        discrimination = ((highest.mean() - lowest.mean()) / points).values

        stats = stats.describe()

        stats.loc['Erreichbare Punkte'] = points
        stats.loc['Schwierigkeit'] = stats.loc['mean']/points
        stats.loc['Diskriminierungsindex'] = discrimination

        stats = stats.rename({'mean': 'Mittelwert', 'std': 'Standardabweichung', '25%': '25%-Quantil',
                              '50%': '50%-Quantil (Median)', '75%': '75%-Quantil', 'min': 'Niedrigste Einzelwertung',
                              'max': 'H'  + chr(246) + 'chste Einzelwertung'})
        stats = stats.rename(columns={'Punkte': 'Alle Aufgaben'})

        stats = stats.round(2)
        return pd.DataFrame(stats.iloc[1:])
    
    def exam_statistics(self):
        """
        DataFrame with statistics for the exam
        """
        failees = (self.grades['Note'] == self.grade_labels[0])
        writers = (self.grades['Note'] != self.grade_labels[-1])
        grades = self.grades[writers]['Note']

        if is_numeric(self.grade_labels[0:-1]):
            stats = pd.to_numeric(grades, errors='coerce').describe()

            # resort rows
            stats = stats.reindex(
                index=['mean', 'std', 'Durchfallquote [%]', 'count', 'min', '25%', '50%', '75%', 'max'])

            # rename rows
            stats = stats.rename({'mean': 'Notenschnitt', 'std': 'Standardabweichung', '25%': '25%-Quantil',
                                  '50%': '50%-Quantil (Median)', '75%': '75%-Quantil', 'min': 'Beste Note',
                                  'max': 'Schlechteste Note', 'count': 'Mitschreiber'})

            # add failing statistics
            stats['Durchfallquote [%]'] = 100 * float(grades[failees].shape[0]) / self.grades[writers].shape[0]

            # empty header name 'Note' -> ''
            stats = stats.rename('')
        else:
            stats = pd.DataFrame({'': [0, 0]}, index=['Mitschreiber', 'Durchfallquote [%]'])
            stats.loc['Mitschreiber'] = self.grades[writers].shape[0]
            stats.loc['Durchfallquote [%]'] = 100 * float(grades[failees].shape[0]) / self.grades[writers].shape[0]

        return pd.DataFrame(stats).round(2)

    def histogram(self, grades=False, relative=False, cumulative=False, **kwargs):
        """
        plot a histogram
        :param grades: If True, a histogram of grades is plotted, otherwise a histogram of points
        :param relative: If True, the y-Axis will be normalized
        :param cumulative: If True, a cumulative histogram is produced
        :param kwargs: Additional keyword arguments passed to the plot call of the pandas DataFrame
        :return:
        """
        if grades:

            sums = pd.Series(0, index=self.grade_labels)\

            if is_numeric(self.grade_labels[0:-1]):
                sums = sums.sort_index()

            sums_actual = self.grades['Note'].value_counts()
            for key in list(sums_actual.index):
                sums[key] = sums_actual[key]

            xlabel = 'Note'
        else:
            sums = self.grades['Punkte'].value_counts().sort_index(ascending=False)
            xlabel = 'Punkte'

        if relative:
            sums = 100*sums/sums[:].sum()
            ylabel = 'Prozent'
        else:
            ylabel = 'Anzahl'

        if cumulative:
            sums = sums.cumsum()
            title = 'Kummuliertes Histogramm, ' + xlabel
        else:
            title = 'Histogramm, ' + xlabel

        sums_max = sums[:].max()

        ax = sums.plot(color='blue', kind="bar", sort_columns=False, **kwargs)

        if relative:
            ax.set_yticks([i * sums_max / 100 for i in range(0, 101, 5)])
        else:
            ax.yaxis.set_major_locator(MaxNLocator(integer=True))

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.grid(axis='y')

        return ax
    
    def write_csv(self, out_path, columns=None, only_graded=False, **kwargs):
        """
        Export the exam to csv

        :param out_path: file of the csv file
        :param columns: optional list of column names to be included in the export
        :param only_graded: write out only those rows that have been graded
        :return:
        """
        if columns is None:
            columns = list(self.grades)

        if only_graded:
            out = self.get_graded()
        else:
            out = self.grades

        out.to_csv(out_path, columns=columns, index=False, **kwargs)


def multi_column_df_display(list_dfs, cols=3):
    """"
    Display two (or more) pandas tables next to each other in Jupyter notebooks.
    """
    html_table = "<table style='width:100%; border:0px'>{content}</table>"
    html_row = "<tr style='border:10px'>{content}</tr>"
    html_cell = "<td style='width:{width}%;vertical-align:top;border:0px'>{{content}}</td>"
    html_cell = html_cell.format(width=100 / cols)

    cells = [html_cell.format(content=df.to_html()) for df in list_dfs]
    cells += (cols - (len(list_dfs) % cols)) * [html_cell.format(content="")]  # pad
    rows = [html_row.format(content="".join(cells[i:i + cols])) for i in range(0, len(cells), cols)]
    display(HTML(html_table.format(content="".join(rows))))


@click.command()
@click.argument('file_name', default='template')
@click.option('--to', default='html', help='Export format, can be html (default) or pdf')
@click.option('--run/--no-run', default=True, help='Execute the notebook before export (default) or not')
def export_notebook(file_name, to='html', run=True):
    """
    export a jupyter notebook to html or pdf
    """
    notebook_content = io.open(file_name, 'r', encoding='utf8').read()
    notebook = nbformat.reads(notebook_content, as_version=4)

    if run:
        ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
        ep.preprocess(notebook, {'metadata': {'path': '.'}})

    exporter = HideCodeHTMLExporter()
    if to == 'pdf':
        exporter = HideCodePDFExporter()

    basename = os.path.splitext(file_name)[0]

    (body, resources) = exporter.from_notebook_node(notebook)
    with io.open(basename + '.' + to, 'w', encoding='utf8') as file:
        file.write(body)


@click.command()
@click.argument('file_name', default='template')
def create(file_name):
    """
    copy template notebook to current working directory
    """
    template_notebook = pkg_resources.read_text(template, 'template.ipynb')
    template_csv = pkg_resources.read_text(template, 'exam_in.csv')

    with io.open(file_name + '.ipynb', 'w', encoding='utf8') as file:
        file.write(template_notebook)
    with io.open('exam_in.csv', 'w', encoding='utf8') as file:
        file.write(template_csv)


@click.group()
@click.version_option()
def cli():
    """
    HBRS Grader: A Jupyter notebook workflow for grading exams.
    """
    pass


cli.add_command(create)
cli.add_command(export_notebook)


if __name__ == '__main__':
    cli()
